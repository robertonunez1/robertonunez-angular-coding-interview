import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe();


   questionsData : QuestionModel[] = []
   goToAnswers: boolean = false;
   showResults: string = "";

  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
   
    this.questions$.subscribe(res => {
      this.questionsData = res;
    })


  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);

    // console.log(this.questionsData)
    let questionsCount = this.questionsData.length;
    let userAnswers = 0;
    userAnswers = this.questionsData.filter(el => el.selectedId !== undefined).length;

     this.goToAnswers = (userAnswers === questionsCount)? true : false;


  }

  showTriviaResults(): void {
    this.showResults = this.getValidAnswers();
  }


  getValidAnswers(): string {

    let validCount = 0;
    let invalidCount = 0;

    this.questionsData.forEach(el => {
     let selectedAnswerId = el.selectedId;
      let valid = el.answers.find(a => a._id === selectedAnswerId)?.isCorrect;

     if(valid){
      validCount++;
     }else{
      invalidCount++;

     } 

    });

    return "Valid answers: "  + validCount + "; Invalid: " + invalidCount;
  
  }

}
